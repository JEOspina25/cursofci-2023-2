from IntegracionMonteCarlo import MonteCarlo
from sympy import *

if __name__ == '__main__':
    
    
    a = 0
    b =1# pi
    n = 10000
    x = Symbol('x')
    f = tanh(x/7)#x**2 * cos(x)
    
    solve = MonteCarlo(f,n,a,b)

    solve.plot_comparar()
    print('Integral hallada por método de Montecarlo, para {} iteraciones: {}'.format(n,round(solve.integrate(n),3)))
    print('Integral analítica: {}'.format(round(solve.integrate_analytic(),3)))
    
