import numpy as np
import matplotlib.pyplot as plt
import sympy as sp


class MonteCarlo:
    
    """
    Esta clase implementa el Método de Montecarlo para calcular la integral definida de una función f en un intervalo dado y compara los resultados con la solución analítica.
    
    Params

    - f : Función a integrar
    - N : Número de iteraciones 
    - a : Límite inferior de intérvalo
    - b : Límite superior del intérvalo
    
    Return
    
    Esta clase devuelve un array que representa la solución de la integral mediante el 
    Método de Montecarlo y genera una gráfica comparativa entre esta solución y la solución analítica.
    
    """

    
    def __init__(self, f, N, a, b):
      self.f = f
      self.N = N
      self.a = a
      self.b = b
      
      
    def integrate(self,n):
        x = sp.Symbol('x')
        
        try:
            f_num = sp.lambdify(x, self.f, "numpy")

            xp = np.random.uniform(self.a, self.b,n)
            
            total = np.vectorize(lambda xp: f_num(xp))(xp).sum()

            return float(((self.b - self.a) / n) * total)
    
        except (TypeError, ValueError):
            print("Error: La función o los valores ingresados no son válidos. \n Debe ser una función del tipo simbólica")
            
        except Exception as e:
            print("Error inesperado:", e)
        
        
    def integrate_analytic(self):
        try:
            x = sp.symbols('x')

            sol = sp.integrate(self.f, (x,self.a,self.b))
            
            return float(sol)
            
        except (TypeError, ValueError):
            print("Error: La función o los valores ingresados no son válidos. \n Debe ser una función del tipo simbólica")
            
        except Exception as e:
            print("Error inesperado:", e)

        
    def plot_comparar(self):
        
        try:
            solve_montecarlo = []
            
            n = np.arange(1,self.N,2)
            for i in range(1,len(n)+1):
                solve_montecarlo.append(self.integrate(i))
                
            solve_anal = self.integrate_analytic()
            
            plt.plot(n, solve_montecarlo, label='Montecarlo', color='red')
            plt.hlines(solve_anal,0,self.N, label='Analítica', color='green')
            plt.legend()
            plt.title('Comparativa método de solución integral')
            plt.xlabel('Número de iteraciones')
            plt.ylabel('Valor de la integral')
            plt.savefig('GraficaComparativa.png')
            plt.show()
        except Exception as e:
            print(f"Se ha producido un error: {e}")