from MontecarloSimulation import Sist_termodinámico


if __name__ == '__main__':
    
    num_part = 100
    temp = 50
    num_pasos = 500  

    simulacion = Sist_termodinámico(num_part,temp,num_pasos)

    simulacion.Run()


