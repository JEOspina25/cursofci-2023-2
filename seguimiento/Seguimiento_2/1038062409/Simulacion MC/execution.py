from MCS import TransporteFotones

if __name__ == '__main__':
    
    #Para nuestro caso de estudio suponemos que los 
    #fotones ingresan perpendicularmente por el origen
    
    #Definimos el numero de fotones
    #para la simulación de la difusión de fotones
    
    #asignar valor entero positivo mayor a 1000000 
    #para comenza a ver resultados significativos
    nphotons = 10000000
    
    #Defnimos el numero de fotones
    #para mostrar sus trayectorias
    
    #En esta caso todo lo contrario a la anterior
    #Es decir, asignar un valor relativamente pequeño
    nphotons3d = 50
    
    
    #Definimos propiedades del medio
    sigma_a = 1    #coeficiente de absorción
    sigma_s = 3    #coeficiente de scattering
    
    #parametro de anisotropia (g = 0 isotropico, g = 1 unidireccional)
    g = 0.8      #asignar valor entre 0 y 1 
    d = 0.2       #espesor del medio (mayor a 0)
    
    #manejo de excepciones
    run = TransporteFotones(nphotons3d,sigma_a,sigma_s,d,g)
    run.run()
    
    #creamos la simulación de la difusión de fotones
    sim = TransporteFotones(nphotons,sigma_a,sigma_s,d,g)
    imag = sim.Imagen()
        
    #creamos la simulación
    #mostramos la trayectoria de los fotones
    sim3d = TransporteFotones(nphotons3d,sigma_a,sigma_s,d,g)
    imag3d = sim3d.Imagen3D()
    
    