import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import random

class TransporteFotones(object):
    def __init__(self,nphotons,sigma_a,sigma_s,d,g):
        self.nphotons = nphotons
        self.sigma_a = sigma_a
        self.sigma_s = sigma_s
        self.sigma_t = sigma_a + sigma_s
        self.d = d
        self.g = g
        self.x = 0
        self.y = 0
        self.z = 0
        self.ux = 0
        self.uy = 0
        self.uz = 1
        self.m = 10
        
    def g_costheta(self):
        """
        Este metodo nos permite calcular el cos(theta) para la dispersion isotropica
        """
        if self.g == 0:
            return 1 - 2 * random.random()
        
        mu = (1 - self.g**2) / (1 - self.g + 2 * self.g * random.random())
        return (1 + self.g**2 - mu**2) / (2 * self.g)
    
    #Esta parte del codigo esta en revisión
    # def spin(self):
    #     """
    #     Este metodo nos permite calcular la dispersion de Henyey-Greenstein
    #     """
    #     #Polar Angle (Henyey-Greenstein scattering)
    #     costheta = self.g_costheta()
        
    #     #Azimuthal Angle (uniformly distributed)
    #     phi    = 2 * np.pi * random.random()
    #     cosphi = np.cos(phi)
        
    #     #nueva direccion
    #     sintheta = np.sqrt(1 - costheta**2)
    #     sinphi   = np.sin(phi)
    
    #     #si la direccion de la particula es paralela al eje z
    #     if self.uz == 1:
    #         self.ux = sintheta * cosphi
    #         self.uy = sintheta * sinphi
    #         self.uz = costheta
            
    #     #si la direccion de la particula es antiparalela al eje z
    #     elif self.uz == -1:
    #         self.ux = sintheta * cosphi
    #         self.uy = -sintheta * sinphi
    #         self.uz = -costheta
            
    #     else: 
    #         part = np.sqrt(1 - self.uz**2)
    #         self.ux = sintheta * (self.ux * self.uz * cosphi - self.uy * sinphi) / part + self.ux * costheta
    #         self.uy = sintheta * (self.uy * self.uz * cosphi + self.ux * sinphi) / part + self.uy * costheta
    #         self.uz = -sintheta * cosphi * part + self.uz * costheta
            
            
    #     return self.ux , self.uy , self.uz 
    
    def Spin(self,mu_x, mu_y, mu_z):

        #Polar Angle (Henyey-Greenstein scattering)
        costheta = self.g_costheta()
        
        #Azimuthal Angle (uniformly distributed)
        phi    = 2 * np.pi * random.random()
        cosphi = np.cos(phi)
        
        #New trajectory
        sintheta = np.sqrt(1.0 - costheta**2) 
        sinphi   = np.sin(phi) #np.sqrt(1.0 - cosphi**2) if phi < np.pi else 0 #
        
        #si la direccion de la particula es paralela al eje z
        if  mu_z == 1.0:
            mu_x = sintheta * cosphi
            mu_y = sintheta * sinphi
            mu_z = costheta
        
        #si la direccion de la particula es antiparalela al eje z
        elif mu_z == -1.0:
            
            mu_x = sintheta * cosphi
            mu_y = -sintheta * sinphi
            mu_z = -costheta
            
        else:
            
            denom = np.sqrt(1.0 - mu_z**2)
            ux = sintheta * (mu_x * mu_z * cosphi - mu_y * sinphi) / denom +  mu_x * costheta
            uy = sintheta * (mu_y * mu_z * cosphi + mu_x * sinphi) / denom +  mu_y * costheta
            uz =                                -denom * sintheta * cosphi +  mu_z * costheta 
            
            mu_x, mu_y, mu_z = ux, uy, uz
            
        return mu_x, mu_y, mu_z
          
    def MCSimulation(self):
        #creamos un arreglo para guardar los datos de la simulacion
        lado = 512
        cuadro = np.zeros((lado*lado),dtype=float)
        #tamaño de cada pixel
        pixel = 5

        #coordenadas del recorrido de la particula 
        for _ in range(self.nphotons):
            #peso inicial
            w = 1
            
            x , y ,z     = self.x , self.y , self.z
            ux , uy , uz = self.ux , self.uy , self.uz
                
            while True:                
                #Calculamos el salto libre medio de la particula
                s = -np.log(random.random()) / self.sigma_t
                
                #distancia a la superficie inferior
                dist_inf = 0
                
                if uz > 0: #si la particula se mueve en direccion a la superficie inferior
                    dist_inf = (self.d - z) / uz 
                     
                elif uz < 0: #si la particula se mueve en direccion a la superficie superior
                    dist_inf = - z / uz 
                
                #si distancia al salto libre medio es mayor que la distancia a la superficie inferior
                if s > dist_inf:
                    #(x,y) -> (xi,yi) 
                    xi = int(((x+pixel/2)/pixel) * lado)
                    yi = int(((y+pixel/2)/pixel) * lado)
                    
                    if uz > 0 and 0<=xi<lado and 0<=yi<lado:# and x!=0 and y!=0:
                        cuadro[xi + yi * lado] += w 
                        break 
                
                    break
                
                #como el salto libre medio es menor que la distancia a la superficie inferior
                #calculamos la nueva posicion de la particular
                x,y,z = x + s * ux , y + s * uy , z + s * uz
                
                #calculamos la nueva direccion de la particula
                ux,uy,uz = self.Spin(ux,uy,uz)
            
                #calculamos el peso de la particula
                dw = self.sigma_a / self.sigma_t
                w -= dw
                
                w = max(0.0,w)
                
                if w< 0.001: #Russian Roulette
                    if random.random() < (0.1/self.m):
                        break #la particula se absorbe
                    else:
                        w *= self.m #la particula continua su camino con un peso mayor
        return cuadro
    
    def Imagen(self):
        """
        Este metodo nos permite crear la imagen de la simulacion
        """
        cuadro = self.MCSimulation()
        img = Image.fromarray(cuadro.reshape(512,512))
        plt.imshow(img,cmap='gray')
        #plt.xlim(208, 304)
        #plt.ylim(208, 304)
        plt.xlim(128, 384)
        plt.ylim(128, 384)
        plt.axis('off')
        plt.title('Difusion de fotones alrededor del haz incidente')
        plt.savefig('Simulacion_con_{}_fotones.png'.format(self.nphotons))
        plt.show()
        
    def Simulation3D(self):
        """
        Este metodo se utiliza para crear la simulacion de la trayectoria de los fotones en 3D
        """
        
        #creamos un arreglo para guardar los datos de la simulacion
        lado = 512
        cuadro = np.zeros((lado*lado),dtype=float)
        #tamaño de cada pixel
        pixel = 5
        
        #creamos un diccionario para guardar las coordenadas de cada particula donde la llave es el numero de la particula
        #y el valor es una lista con las coordenadas de la particula
        myphoton = {}

        #coordenadas del recorrido de la particula 
        for i in range(self.nphotons):
            #peso inicial
            w = 1
            
            x , y ,z     = self.x , self.y , self.z
            ux , uy , uz = self.ux , self.uy , self.uz
            myphoton[i] = [(x,y,z)]
            
            while True:      
                #creamos un diccionario donde la llave es el numero de la particula y el valor es una lista con las coordenadas de la particula
                myphoton[i].append((x,y,z))  
                #Calculamos el salto libre medio de la particula
                s = -np.log(random.random()) / self.sigma_t
                
                #distancia a la superficie inferior
                dist_inf = 0
                
                if uz > 0: #si la particula se mueve en direccion a la superficie inferior
                    dist_inf = (self.d - z) / uz 
                     
                elif uz < 0: #si la particula se mueve en direccion a la superficie superior
                    dist_inf = - z / uz 
                
                #si distancia al salto libre medio es mayor que la distancia a la superficie inferior
                if s > dist_inf:
                    #(x,y) -> (xi,yi) 
                    xi = int(((x+pixel/2)/pixel) * lado)
                    yi = int(((y+pixel/2)/pixel) * lado)
                    
                    if uz > 0 and 0<=xi<lado and 0<=yi<lado:# and x!=0 and y!=0:
                        cuadro[xi + yi * lado] += w 
                        break 
                
                    break
                
                #como el salto libre medio es menor que la distancia a la superficie inferior
                #calculamos la nueva posicion de la particular
                x,y,z = x + s * ux , y + s * uy , z + s * uz
                
                #calculamos la nueva direccion de la particula
                ux,uy,uz = self.Spin(ux,uy,uz)
            
                #calculamos el peso de la particula
                dw = self.sigma_a / self.sigma_t
                w -= dw
                
                w = max(0.0,w)
                
                if w< 0.001: #Russian Roulette
                    if random.random() < (0.1/self.m):
                        break #la particula se absorbe
                    else:
                        w *= self.m #la particula continua su camino con un peso mayor
                
                         
        return myphoton
    
    def Imagen3D(self):
        """
        Este metodo nos permite crear la imagen de la trayectoria de cada particula dentro de la muestra en 3D
        """
        fig  = plt.figure()
        ax   = fig.add_subplot(111, projection='3d')
        data = self.Simulation3D()
        
        max_x =1  # Inicializar el máximo en x como 1
        max_y =1  # Inicializar el máximo en y como 1
        
        # Iterar a través de las claves (i) y las listas de coordenadas para graficar
        for i, coordinates in data.items():
            x, y, z = zip(*coordinates)  # Separa las coordenadas en listas x, y y z
            ax.scatter(x, y, z, label=f'i = {i}')  # Graficar los puntos
            ax.plot(x, y, z, label=f'i = {i}', linestyle='-', marker='o')
            max_x = max(max(x), max_x)  # Encontrar el máximo en x
            max_y = max(max(y), max_y)  # Encontrar el máximo en y

        # Crear un plano en Z = 0
    
        x_plane, y_plane = np.meshgrid(np.arange(-max_x*10, max_x*10, max_x/10), np.arange(-max_y*10, max_y*10, max_y/10))
        z_plane = np.zeros_like(x_plane)
        ax.plot_surface(x_plane, y_plane, z_plane, color='gray', alpha=0.5)

        # Crear un plano en Z = b
        z_plane_b = np.full_like(x_plane, self.d)
        ax.plot_surface(x_plane, y_plane, z_plane_b, color='gray', alpha=0.5)
        
        ax.view_init(azim=0, elev=185)
        
        #colocamos texto en el grafico en la coordenada (0,0,0)
        ax.text(0, 0, 0, 'Fuente', color='black')
        
        # Configurar el gráfico
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        plt.title('Trayectoria de los fotones dentro del medio') 
        plt.savefig('Trayectoria_con_{}_fotones.png'.format(self.nphotons))
        plt.show()
    
    def run(self):
        """
        Esta funcion ejecuta todos los metodos de la clase intMC para hacer manejo de errores
        """

        if self.nphotons <= 0:
            raise ValueError('El numero de fotones debe ser mayor a 0')
        
        if self.sigma_a < 0:
            raise ValueError('El coeficiente de absorcion debe ser mayor o igual a 0')
        
        if self.sigma_s < 0:
            raise ValueError('El coeficiente de scattering debe ser mayor o igual a 0')
        
        if self.d <= 0:
            raise ValueError('El espesor del medio debe ser mayor a 0')
        
        if self.g < 0 or self.g > 1:
            raise ValueError('El parametro de anisotropia debe estar entre 0 y 1')
        
        try:
            return None
        
        except ValueError as ve:
            print(f"Error al generar imagen(cambiar parametros de entrada): {ve}")
        except Exception as e:
            print(f"Ocurrió un error inesperado (cambiar parametros de entrada): {e}")
            
