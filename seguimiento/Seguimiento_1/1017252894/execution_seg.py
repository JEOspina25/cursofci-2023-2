import numpy as np
import matplotlib.pyplot as plt
import clase 


"""
NOTA

5.0

- Muy buen trabajo

"""

if __name__ == "__main__":
    
    q = -1.6e-19 # [C]
    B =0# 600e-6   # [T]
    m = 9.1e-31  # [kg]
    Ek =0.3    # [eV]
    theta = 90   # [grados]
    
    sol = clase.magnetico(q,B,m,Ek,theta)

    sol.grafica()

    