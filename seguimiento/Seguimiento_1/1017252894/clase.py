import numpy as np
import matplotlib.pyplot as plt

class magnetico:
    """
    Esta clase resuelve el problema de una partícula cargada en un campo magnético
    parámetros 

        q : Carga eléctrica [C] Valor real
        B : Campo magnético [T] Valor real
        m : Masa [Kg] Valor mayor que cero
       Ek : Energía cinética [eV] Valor mayor que cero
    theta : angulo respecto al eje z B [grados] entre 0 y 180
      phi : angulo polar [grados] entre 0 y 2pi
      R_0 : lista de tres valores con la posiciona inicial x,y,z [m] Valor real
          
    """

    def __init__(self,q,B,m,Ek,theta,phi=0,R_0=[0,0,0]):
        if (m<=0):print("El valor de m debe ser mayor que cero, reinicie el programa");exit()
        if (Ek<0):print("El valor de la energia debe ser positivo, reinicie el programa");exit()

        self.q = q
        self.B = B
        self.m = m
        self.v = np.sqrt(2*Ek*1.6e-19/m) #Se pasa de eV a Julios
        self.theta = theta *np.pi/180 # Se pasa a radianes
        self.phi = phi *np.pi/180

        self.X_0 = R_0[0]
        self.Y_0 = R_0[1]
        self.Z_0 = R_0[2]

        self.Wc = q*B/m
    
    def pos(self,t):
        """
        Este método calcula la posición de la partícula en un t especifico
        """
        Wc = self.Wc
        v_0x = self.v*np.sin(self.theta)*np.cos(self.phi) # Velocidad inicial
        v_0y = self.v*np.sin(self.theta)*np.sin(self.phi)
        v_0z = self.v*np.cos(self.theta)

        if (Wc!=0):
            # Veloidad en funcion de t
            x = (v_0x*np.sin(Wc*t) + v_0y*(1-np.cos(Wc*t)))/Wc + self.X_0
            y = (v_0y*np.sin(Wc*t) + v_0x*(np.cos(Wc*t)-1))/Wc + self.Y_0
        else:
            # Si Wc = 0, entonces significa que no hay campo magnético o la partícula no tiene cargar eléctrica
            # por lo tanto el movimiento de la partícula no se altera
            x = v_0x*t + self.X_0
            y = v_0y*t + self.Y_0
            
        z = v_0z*t + self.Z_0
            
        return [x,y,z]
    
    def grafica(self,ti=0,tf=0.0000003,N=1000):
        """
        Método para Hacer la grafica
        """
        #t = np.arange(0,10000,0.01)
        t = np.linspace(ti,tf,N)

        pos_x, pos_y, pos_z = self.pos(t)

        fig = plt.figure(figsize=(7,7)).add_subplot(projection='3d')
        fig.plot(pos_x,pos_y,pos_z,'--')
        fig.plot(pos_x[0],pos_y[0],pos_z[0],'r*',label="Posición inicial")
        fig.plot(pos_x[-1],pos_y[-1],pos_z[-1],'bo',label="Posición en t = {}".format(t[-1]))
        fig.set_title('Trayectoria de un electrón en un campo magnético')
        fig.set_xlabel('X(t) [m]')
        fig.set_ylabel('Y(t) [m]')
        fig.set_zlabel('Z(t) [m]')
        plt.grid()
        plt.legend()
        plt.savefig("Grafica")
        #plt.show()

        