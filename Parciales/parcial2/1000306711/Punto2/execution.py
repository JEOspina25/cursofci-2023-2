from ising import IsingModel

L=4 #Longitud de la cuadrícula
N=1000 #Número de iteraciones MonteCarlo
ti=0.5 #Temperatura inicial
tf=5 #Temperatura final
Nt=10 #Número de temperaturas

if __name__ == "__main__":
    ising=IsingModel(L,N,ti,tf,Nt)
    ising.magnetization() #Plot magnetización
    ising.heat() #Plot calor específico