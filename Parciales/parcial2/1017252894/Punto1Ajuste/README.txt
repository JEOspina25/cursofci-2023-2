Instalar la libreria con:
    pip install FdoJarParcial2

Para importar:
    from FdoJarParcial2.FdoJarParcial2 import SuavKernel 

Para usarla:

Inicializar la clase
    Solucion = SuavKernel(Nombre_data,h)

Para graficar el kernel Gaussiano
    Solucion.SuavizadoGauss()

Para los demas kernels 
    Solucion.SuavizadoEpanechnikov()
    Solucion.SuavizadoTriCube()

Cada metodo hace un plot pero no lo guarda

Para guardar todo lo que hay graficado 
    Solucion.GuardarFigura("Grafica")

